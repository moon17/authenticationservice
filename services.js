const {
    sendRequest
} = require('./http-client');

const getRoles = async () => {
    console.info(`Sending request to IO for all roles ...`);
    
    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/roles`
    }

    const roles = await sendRequest(options);

    return roles;
};

const getRoleById = async (id) => {
    console.info(`Sending request to IO for role ${id} ...`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/roles/${id}`
    }

    const role = await sendRequest(options);

    return role;
};

const addRole = async (value) => {
    console.info(`Sending request to IO to add role with name ${value} ...`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/roles`,
        method: 'POST',
        data: {
            value
        }
    }

    const id = await sendRequest(options);

    return id;
}; 

const getUsers = async () => {
    console.info(`Sending request to IO for all users ...`);
    
    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/users`
    }

    const users = await sendRequest(options);

    return users;
};

const getUserById = async (id) => {
    console.info(`Sending request to IO for user ${id} ...`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/users/id=${id}`
    }

    const user = await sendRequest(options);

    return user;
};

const getUserByUsername = async (username) => {
    console.info(`Sending request to IO for user ${username} ...`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/users/username=${username}`
    }

    const user = await sendRequest(options);

    return user;
};

const addUser = async (username, password, role_id) => {
    console.info(`Sending request to IO to add user with username ${username}, password ${password} and role_id ${role_id}...`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/users`,
        method: 'POST',
        data: {
            username,
            password,
            role_id
        }
    }

    const id = await sendRequest(options);

    return id;
};

const updateUserRole = async (id, role_id) => {
    console.info(`Sending request to IO to update user with id ${id} with role_id ${role_id}...`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/users/${id}/role/${role_id}`,
        method: 'PUT',
        data: {
            id,
            role_id
        }
    }

    const user = await sendRequest(options);

    return user;
};

const getByUsernameWithRole = async (username) => {
    console.info(`Getting user with username ${username}`);

    const options = {
        url: `http://${process.env.IO_SERVICE_API_ROUTE}/users/:username`,
        method: 'GET',
        data: {
            username
        }
    }
    
    const user = await sendRequest(options);

    return user;
};

module.exports = {
    getRoles,
    getRoleById,
    addRole,
    getUsers,
    getUserById,
    getUserByUsername,
    addUser,
    updateUserRole,
    getByUsernameWithRole
}
