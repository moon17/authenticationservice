const ServerError = require('../Models/ServerError.js');

const {
    verifyAndDecodeData
} = require('../Security/JWT');

const authorizeAndExtractTokenAsync = async (req, res, next) => {
    if (!req.headers.authorization) {
        throw new ServerError('No authorization header provided', 401);
    }
    const token = req.headers.authorization.split(" ")[1];

    const decoded = await verifyAndDecodeData(token);

    req.user = decoded;
    next();
};

module.exports = {
    authorizeAndExtractTokenAsync
}
