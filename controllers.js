const Router = require('express').Router();

const {
    ServerError
} = require('./errors');

const UsersManager = require('./Managers/UsersManager.js');

const {
    UserBody,
    UserRegisterResponse,
    UserLoginResponse
} = require ('./Models/User.js');
const ResponseFilter = require('./Filters/ResponseFilter.js');
const JWTFilter = require('./Filters/JWTFilter.js');
const AuthorizationFilter = require('./Filters/AuthorizationFilter.js');

const {
    getRoles,
    getRoleById,
    addRole,
    getUsers,
    getUserById,
    getUserByUsername,
    addUser,
    updateUserRole,
    getByUsernameWithRole
} = require('./services.js');


Router.get('/', async (req, res) => {
    
    const roles = await getRoles();

    res.json(roles);
});

Router.get('/:id', async (req, res) => {

    const {
        id
    } = req.params;

    const role = await getRoleById(id);

    res.json(role);
});

Router.post('/', async (req, res) => {

    const {
        value
    } = req.body;

    if (!value) {
        throw new ServerError('No value provided!', 400);
    }

    const id = await addRole(value);

    res.json(id);
});

Router.get('/', JWTFilter.authorizeAndExtractTokenAsync, AuthorizationFilter.authorizeRoles("ADMIN"), async (req, res) => {
    
    const users = await getUsers();

    res.json(users);
});

Router.get('/id=:id', async (req, res) => {

    const {
        id
    } = req.params;

    const user = await getUserById(id);

    res.json(user);
});

Router.get('/username=:username', async (req, res) => {

    const {
        username
    } = req.params;

    const user = await getUserByUsername(username);

    res.json(user);
});

Router.post('/', async (req, res) => {

    const {
        username,
        password,
        role_id
    } = req.body;

    if (!username) {
        throw new ServerError('No username provided!', 400);
    }
    if (!password) {
        throw new ServerError('No password provided!', 400);
    }
    if (!role_id) {
        throw new ServerError('No role_id provided!', 400);
    }

    const id = await addUser(username, password, role_id);

    res.json(id);
});

Router.put('/:id/role/:role_id', JWTFilter.authorizeAndExtractTokenAsync, AuthorizationFilter.authorizeRoles("ADMIN"), async (req, res) => {

    const {
        id,
        role_id
    } = req.params;

    const user = await updateUserRole(id, role_id);

    res.json(user);
});

Router.get('/:username', async (req, res) => {

    const {
        username
    } = req.params;

    const user = await getByUsernameWithRole(username);

    res.json(user);
});

Router.post('/register', async (req, res) => {

    const userBody = new UserBody(req.body);
    const user = await UsersManager.register(userBody.Username, userBody.Password);

    ResponseFilter.setResponseDetails(res, 201, new UserRegisterResponse(user));
});

Router.post('/login', async (req, res) => {

    const userBody = new UserBody(req.body);
    const userDto = await UsersManager.authenticate(userBody.Username, userBody.Password);
    const user = new UserLoginResponse(userDto.Token, userDto.Role);

    ResponseFilter.setResponseDetails(res, 200, user);
});

module.exports = Router;
